package com.example.project.chatGPTtask2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.example.project.chatGPTtask2.models.Author;
import com.example.project.chatGPTtask2.models.Book;
import com.example.project.chatGPTtask2.models.Genre;
import com.example.project.chatGPTtask2.repositories.BookRepository;
import com.example.project.chatGPTtask2.services.impl.BookServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class BookServiceTests {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookServiceImpl bookService;

    @Test
    public void testGetAllBooks() {
        // Mock data
        Book book1 = new Book("Title 1", new Author("Author 1"), new Genre("Genre 1"), 10.0, 5);
        Book book2 = new Book("Title 2", new Author("Author 2"), new Genre("Genre 2"), 12.0, 8);
        when(bookRepository.findAll()).thenReturn(Arrays.asList(book1, book2));

        // Test
        List<Book> books = bookService.getAllBooks();
        assertEquals(2, books.size());
        assertEquals("Title 1", books.get(0).getTitle());
        assertEquals("Title 2", books.get(1).getTitle());
    }

    @Test
    public void testGetBookById() {
        // Mock data
        Book book = new Book("Title", new Author("Author"), new Genre("Genre"), 10.0, 5);
        when(bookRepository.findById(1L)).thenReturn(Optional.of(book));

        // Test
        Book result = bookService.getBookById(1L);
        assertNotNull(result);
        assertEquals("Title", result.getTitle());
    }

    @Test
    public void testSaveBook() {
        // Mock data
        Book book = new Book("Title", new Author("Author"), new Genre("Genre"), 10.0, 5);
        when(bookRepository.save(book)).thenReturn(book);

        // Test
        Book savedBook = bookService.saveBook(book);
        assertNotNull(savedBook);
        assertEquals("Title", savedBook.getTitle());
    }

    @Test
    public void testUpdateBook() {
        // Mock data
        Book book = new Book("Title", new Author("Author"), new Genre("Genre"), 10.0, 5);
        when(bookRepository.save(book)).thenReturn(book);

        // Test
        Book updatedBook = bookService.saveBook(book);
        assertNotNull(updatedBook);
        assertEquals("Title", updatedBook.getTitle());
    }

    @Test
    public void testDeleteBookById() {
        // Test
        assertDoesNotThrow(() -> bookService.deleteBookById(1L));
    }

    @Test
    public void testSearchBooks() {
        // Mock data
        Book book1 = new Book("Java Programming", new Author("John Doe"), new Genre("Programming"), 29.99, 10);
        Book book2 = new Book("Introduction to Spring Boot", new Author("Jane Smith"), new Genre("Programming"), 24.99, 8);
        when(bookRepository.searchBooks("Java")).thenReturn(Arrays.asList(book1));

        // Test
        List<Book> books = bookService.searchBooks("Java");
        assertEquals(1, books.size());
        assertEquals("Java Programming", books.get(0).getTitle());
    }

}
