package com.example.project.chatGPTtask2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.example.project.chatGPTtask2.models.Genre;
import com.example.project.chatGPTtask2.repositories.GenreRepository;
import com.example.project.chatGPTtask2.services.impl.GenreServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class GenreServiceTests {

    @Mock
    private GenreRepository genreRepository;

    @InjectMocks
    private GenreServiceImpl genreService;

    @Test
    public void testGetGenreById() {
        // Mock data
        Genre genre = new Genre("Fiction");
        when(genreRepository.findById(1L)).thenReturn(Optional.of(genre));

        // Test
        Genre result = genreService.getGenreById(1L);
        assertNotNull(result);
        assertEquals("Fiction", result.getName());
    }

    @Test
    public void testSaveGenre() {
        // Mock data
        Genre genre = new Genre("Fiction");
        when(genreRepository.save(genre)).thenReturn(genre);

        // Test
        Genre savedGenre = genreService.saveGenre(genre);
        assertNotNull(savedGenre);
        assertEquals("Fiction", savedGenre.getName());
    }

    @Test
    public void testUpdateGenre() {
        // Mock data
        Genre genre = new Genre("Fiction");
        when(genreRepository.save(genre)).thenReturn(genre);

        // Test
        Genre updatedGenre = genreService.saveGenre(genre);
        assertNotNull(updatedGenre);
        assertEquals("Fiction", updatedGenre.getName());
    }

    @Test
    public void testDeleteGenreById() {
        // Test
        assertDoesNotThrow(() -> genreService.deleteGenreById(1L));
    }

    @Test
    public void testGetAllGenres() {
        // Mock data
        Genre genre1 = new Genre("Fiction");
        Genre genre2 = new Genre("Science Fiction");
        when(genreRepository.findAll()).thenReturn(Arrays.asList(genre1, genre2));

        // Test
        List<Genre> genres = genreService.getAllGenres();
        assertEquals(2, genres.size());
        assertEquals("Fiction", genres.get(0).getName());
        assertEquals("Science Fiction", genres.get(1).getName());
    }
}
