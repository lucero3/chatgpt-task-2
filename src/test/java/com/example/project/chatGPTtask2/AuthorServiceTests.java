package com.example.project.chatGPTtask2;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.example.project.chatGPTtask2.models.Author;
import com.example.project.chatGPTtask2.repositories.AuthorRepository;
import com.example.project.chatGPTtask2.services.impl.AuthorServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AuthorServiceTests {

    @Mock
    private AuthorRepository authorRepository;

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Test
    public void testGetAuthorById() {
        // Mock data
        Author author = new Author("John Doe");
        when(authorRepository.findById(1L)).thenReturn(Optional.of(author));

        // Test
        Author result = authorService.getAuthorById(1L);
        assertNotNull(result);
        assertEquals("John Doe", result.getName());
    }

    @Test
    public void testSaveAuthor() {
        // Mock data
        Author author = new Author("John Doe");
        when(authorRepository.save(author)).thenReturn(author);

        // Test
        Author savedAuthor = authorService.saveAuthor(author);
        assertNotNull(savedAuthor);
        assertEquals("John Doe", savedAuthor.getName());
    }

    @Test
    public void testUpdateAuthor() {
        // Mock data
        Author author = new Author("John Doe");
        when(authorRepository.save(author)).thenReturn(author);

        // Test
        Author updatedAuthor = authorService.saveAuthor(author);
        assertNotNull(updatedAuthor);
        assertEquals("John Doe", updatedAuthor.getName());
    }

    @Test
    public void testDeleteAuthorById() {
        // Test
        assertDoesNotThrow(() -> authorService.deleteAuthorById(1L));
    }

    @Test
    public void testGetAllAuthors() {
        // Mock data
        Author author1 = new Author("John Doe");
        Author author2 = new Author("Jane Smith");
        when(authorRepository.findAll()).thenReturn(Arrays.asList(author1, author2));

        // Test
        List<Author> authors = authorService.getAllAuthors();
        assertEquals(2, authors.size());
        assertEquals("John Doe", authors.get(0).getName());
        assertEquals("Jane Smith", authors.get(1).getName());
    }
}
