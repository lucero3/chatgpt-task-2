package com.example.project.chatGPTtask2.services;

import com.example.project.chatGPTtask2.models.Book;

import java.util.List;

public interface BookService {
    Book saveBook(Book book);
    Book getBookById(Long id);
    List<Book> getAllBooks();
    void deleteBookById(Long id);
    List<Book> searchBooks(String searchTerm);
}