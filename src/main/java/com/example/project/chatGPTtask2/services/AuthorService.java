package com.example.project.chatGPTtask2.services;

import com.example.project.chatGPTtask2.models.Author;

import java.util.List;

public interface AuthorService {
    Author saveAuthor(Author author);
    Author getAuthorById(Long id);
    List<Author> getAllAuthors();
    void deleteAuthorById(Long id);
}
