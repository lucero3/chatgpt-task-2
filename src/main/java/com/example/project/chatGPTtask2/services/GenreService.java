package com.example.project.chatGPTtask2.services;

import com.example.project.chatGPTtask2.models.Genre;

import java.util.List;

public interface GenreService {
    Genre saveGenre(Genre genre);
    Genre getGenreById(Long id);
    List<Genre> getAllGenres();
    void deleteGenreById(Long id);
}