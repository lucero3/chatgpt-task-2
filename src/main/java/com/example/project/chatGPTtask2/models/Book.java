package com.example.project.chatGPTtask2.models;


import lombok.Data;
import jakarta.persistence.*;

@Entity
@Data
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;

    private double price;
    private int quantityAvailable;

    public Book( String title, Author author, Genre genre, double price, int quantityAvailable) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.price = price;
        this.quantityAvailable = quantityAvailable;
    }
}
