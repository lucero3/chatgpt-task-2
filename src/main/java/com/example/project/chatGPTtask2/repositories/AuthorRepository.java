package com.example.project.chatGPTtask2.repositories;

import com.example.project.chatGPTtask2.models.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
