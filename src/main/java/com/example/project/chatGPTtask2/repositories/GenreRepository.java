package com.example.project.chatGPTtask2.repositories;
import com.example.project.chatGPTtask2.models.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}