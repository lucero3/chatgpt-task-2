package com.example.project.chatGPTtask2.repositories;

import com.example.project.chatGPTtask2.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findByTitleContainingIgnoreCase(String title);
    List<Book> findByAuthorNameContainingIgnoreCase(String authorName);
    List<Book> findByGenreNameContainingIgnoreCase(String genreName);
    @Query("SELECT b FROM Book b WHERE lower(b.title) LIKE lower(concat('%', :searchTerm, '%')) OR lower(b.author.name) LIKE lower(concat('%', :searchTerm, '%')) OR lower(b.genre.name) LIKE lower(concat('%', :searchTerm, '%'))")
    List<Book> searchBooks(@Param("searchTerm") String searchTerm);
}
